from mongoengine import *
import datetime
class Thread(Document):
    id = SequenceField(primary_key=True)
    title = StringField()
    email = EmailField()
    author = StringField(default='Anon')
    post = StringField()
    pub_date = DateTimeField(default=datetime.datetime.now())
    image = ImageField()
    metahash = StringField()
    tags = ListField(StringField(max_length=30))
    
    def __unicode__(self):
        return self.title

class Post(Document):
     id = SequenceField(primary_key=True)
     email = EmailField()
     author = StringField(default='Anon')
     post = StringField()
     pub_date = DateTimeField(default=datetime.datetime.now())
     image = ImageField()
     metahash = StringField()
     thread = ReferenceField(Thread)
