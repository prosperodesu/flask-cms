from core.contrib import app
from flask import render_template, Blueprint, redirect, request
from boards.models import Thread, Post
from boards.forms import ThreadForm, ReplyForm
from core.settings import UPLOAD_PATH
import hashlib, uuid
import os
#site_index = Blueprint('index', __name__, template_folder='templates')
#site_thread = Blueprint('threads', __name__, template_folder='templates')
#site_tag = Blueprint('tags', __name__, template_folder='templates')


@app.route('/', methods=['GET', 'POST'])
def index(request):
    form = ThreadForm(request.POST)
    threads = Thread()
    if form.validate_on_submit():
        title = form.title.data
        email = form.email.data
        post = form.post.data
        metahash = uuid.uuid1().hex
        thread = Thread(title=title, email=email, post=post,image=image_data, metahash=metahash)
        thread.save()
        get_thread = Thread.objects(metahash=metahash)
        for thread in get_thread:
            id = thread.id
            print id
        return redirect('/thread/' + str(id))
    threads = Thread.objects()
    return render_template('admin.html', threads=threads, form=form)

@app.route('/thread/<id>', methods=['GET', 'POST'])
def threads(id):
    form = ReplyForm()
    get_thread = Thread.objects(id=id)
    for thread in get_thread:
        metahash = thread.metahash
        thread_id = thread.id
    if form.validate_on_submit():
        email = form.email.data
        post = form.post.data
        image = form.image.object_data
        post = Post(email=email, post=post,image=image, metahash=metahash)
        get_post = Post.objects(metahash=metahash)
        for post_id in get_post:
            id_post = post_id.id
            post.save()
            return redirect('thread/'+str(thread_id)+"#"+str(id_post))
    replies = Post.objects(metahash=metahash)
    return render_template('post.html', form=form,
                           threads=get_thread,
                           replies=replies)

@app.route('/tag/<tag>')
def tags(tag):
    form = ThreadForm()
    get_threads = Thread.objects.filter(tags=tag)
    return render_template('post_tags.html', form=form, threads=get_threads)