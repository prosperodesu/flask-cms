from flask.ext.wtf import Form
from wtforms.fields import TextField, TextAreaField, FileField

class ThreadForm(Form):
    title = TextField('Title')
    email = TextField('Email')
    author = TextField('Author')
    post = TextAreaField('Post')
    media = TextField('Media')
    image = FileField('Image')

class ReplyForm(Form):
    email = TextField('Email')
    author = TextField('Author')
    post = TextAreaField('Post')
    media = TextField('Media')
    image = FileField('Image')