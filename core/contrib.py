from core import app
import logging
from logging import FileHandler
from settings import DEBUG, STATIC, mongodb_settings
from mongoengine import *
from pages.routes import site_index, site_page

if DEBUG == True:
    file_handler = FileHandler("debug.log","a")
    file_handler.setLevel(logging.WARNING)
    app.logger.addHandler(file_handler)
    from werkzeug import SharedDataMiddleware
    import os
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
      '/': STATIC
    })

connect(**mongodb_settings)
app.register_blueprint(site_index)
app.register_blueprint(site_page)