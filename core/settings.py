from admin.contrib import admin
from core.contrib import app
import os
# Run settings
autoreload = True
port = 8000
threaded=True

# App settings
app.secret_key = 'sa2342eda234214'
CSRF_ENABLED = True

#Debug settings
DEBUG = True

# Database Settings(MongoDB)
mongodb_settings = {
    'db':'cms', # Database Name
    # 'username':None,
    # 'password':None,
    # 'host':None,
    # 'port':None
}

# Admin Settings
#admin.register(User, UserModel)

STATIC = os.path.join(os.path.dirname(__file__), 'static')
UPLOAD_PATH = os.path.join(os.path.dirname(__file__), 'upload')