from flask.ext.admin.contrib.mongoengine import ModelView
import os
TEMPLATE_PATH = os.path.join(os.path.dirname(__file__), 'templates')

class NewsModel(ModelView):
    list_display = ('title', 'content', 'pub_date')
    create_template = TEMPLATE_PATH + '\my_edit.html'
    edit_template = TEMPLATE_PATH + '\my_edit.html'
    print edit_template


class SloganModel(ModelView):
    list_display = ('title', 'content', 'pub_date')
