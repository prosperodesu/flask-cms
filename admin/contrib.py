from core import app
import os
from flask.ext.admin import Admin
from views import IndexView
#from boards.models import Thread, Post
from pages.models import News, Slogan, Slider, Menu, StaticPage
from admin.models import NewsModel,SloganModel
from flask.ext.admin.contrib.mongoengine import ModelView
from flask.ext.admin.contrib.fileadmin import FileAdmin


STATIC_PATH = "C:/Users/prospero/PycharmProjects/flask-cms/core/static" # Path for static files
admin = Admin(app, 'Flask-CMS') # Create instance of Admin class

"""Add views and models for admin
admin.add_view(ModelView/BaseView(Model)"""
admin.add_view(IndexView('Home'))
admin.add_view(ModelView(Slogan, 'Slogan', 'Index Page'))
admin.add_view(ModelView(Slider, 'Slider', 'Index Page'))
admin.add_view(ModelView(News, 'News', 'Index Page'))
admin.add_view(ModelView(StaticPage, 'Static Pages'))
admin.add_view(FileAdmin(STATIC_PATH, '/static', 'Static Files'))