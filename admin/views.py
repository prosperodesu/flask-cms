from core import app
import os
from datetime import datetime
from flask.ext.admin import Admin, BaseView, expose

class IndexView(BaseView):
    @expose('/')
    def index(self):
        date_now = datetime.now()
        date_format = date_now.strftime('%d.%m.%Y')
        time_format = date_now.strftime('%I:%M %p')
        disk_space = os.path.getsize('c:\\')
        return self.render('admin.html', date_now=date_format,
                           time_now=time_format,
                           disk_space = disk_space)

