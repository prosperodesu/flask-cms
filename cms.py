from core.contrib import app
from core.settings import autoreload, port, threaded

if __name__ == '__main__':
    app.debug = True
    app.run(port=port, threaded=threaded, use_reloader=autoreload)