from core.contrib import app
from flask import render_template, Blueprint, redirect
from pages.models import News, Slogan, Slider, StaticPage
import _md5
site_index = Blueprint('index', __name__, template_folder='templates')
site_page = Blueprint('page', __name__, template_folder='templates')

@app.route('/')
def index():
    menu_pages = StaticPage.objects()
    news = News.objects()
    slogan = Slogan.objects[:4]
    slider = Slider.objects[:3]
    return render_template('index.html',
                           news=news,
                           slogan=slogan,
                           slider=slider,
                           menu_pages=StaticPage.objects())

@app.route('/page/<url>')
def page(url):
    menu_pages = StaticPage.objects()
    pages = StaticPage.objects.filter(url=url)
    if not pages:
        return redirect('404')
    return render_template('internet.html', pages=pages,
                           menu_pages=menu_pages)

@app.route('/404')
def not_found():
    return render_template('404.html')

