from mongoengine import *
from datetime import datetime

class Menu(Document):
    title = StringField()
    page = ListField(ReferenceField('StaticPage'))

    def __unicode__(self):
        return self.title


class StaticPage(Document):
    title = StringField()
    second_title = StringField(max_length=50)
    content = StringField()
    url = StringField()

    def __unicode__(self):
        return self.title


class Slider(Document):
    content = StringField()
    image = ImageField()
    pub_date = DateTimeField(default=datetime.now())


class News(Document):
    title = StringField()
    content = StringField()
    text = StringField()
    pub_date = DateTimeField(default=datetime.now())


    def __unicode__(self):
        return self.title

class Slogan(Document):
    title = StringField(max_length=20)
    content = StringField()
    pub_date = DateTimeField(default=datetime.now())